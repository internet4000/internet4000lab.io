# website for internet4000.gitlab.io

To deploy push to the `production` branch.

## internet4000.com

> retired domain (only redirects are left temporary)

Used to be in usage, but now we're only using static pages providers for all
apps, without custom domains.

### Current redirects

> Do not use for any special purpose; it's only there for legacy (and url typing
> conveniency), but might break + change at any point.

#### {find,note,vinyl} subdomains

To use redirects in cloudflare (and generally with DNS providers), we'll need.

- A records, are required, for browsers to resolve the page (at cloudlare they also need to be "proxied")

```DNS
internet4000.com 192.0.2.1 Proxied
find 192.0.2.1 Proxied
note 192.0.2.1 Proxied
vinyl 192.0.2.1 Proxied
```

> 129.0.2.1 seems to be a common practise to redirect to "sleeping domain"

Source: https://community.cloudflare.com/t/redirect-page-rule-not-working-without-a-record/395958

- page redirects:

Cloudflare only provides 3 free page rules.

1. `*find.internet4000.com/*`, Forwarding URL (Status Code: 301 - Permanent Redirect, Url: https://internet4000.github.io/find/$2)
2. `*note.internet4000.com/*`, Forwarding URL (Status Code: 301 - Permanent Redirect, Url: https://internet4000.gitlab.io/i4k-notes/$2)
3. `*vinyl.internet4000.com/*`, Forwarding URL (Status Code: 301 - Permanent Redirect, Url: https://internet4000.gitlab.io/vinyl-finder/$2)

#### apex domain

- using a redirect rule with the expression: `(http.host eq "internet4000.com")`, for a static redirect to https://internet4000.gitlab.io

> the subdomain could also use that, and would still need to be declared as DNS A records
